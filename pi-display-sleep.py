from os import popen,system
from time import sleep

state=1 #State of the display 1 On 0 Off

ip="192.168.0.53" #Enter the IP address of the device that should keep the display awake

while True:
    nmap_out=str(popen('nmap -sP '+ip).read()) #nmap command to scan on the given IP address

    sleep(5)    #give nmap 5 seconds to run to prevent screen flicker

    if nmap_out.find('Host is up') == -1:  #looks for the word "latency" in the output
        if state==0 :                   #this nested if makes sure that commands are not repeated
            pass
        else :
            system('sudo bash -c "echo 1 > /sys/class/backlight/rpi_backlight/bl_power"')  #Bash command that turns off the display
            state=0                             #Updating the display state variable

    elif nmap_out.find('Host is up') >= 1:
        if state==1:
            pass
        else :
            system('sudo bash -c "echo 0 > /sys/class/backlight/rpi_backlight/bl_power"') #Bash command to turn on the display
            state=1

    sleep(30) #Scan rate in seconds
